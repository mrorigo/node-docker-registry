/**
   This file is part of node-docker-registry.

   node-docker-registry is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   node-docker-registry is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with node-docker-registry.  If not, see <http://www.gnu.org/licenses/>.
*/

module.exports = function(server, config, log) {

    var JWT = require('jwt-async');
    var jwt = new JWT();

    jwt.setSecret(config.auth.jwt_secret);

    server.use(function _auth_v2(req, res, next) {
        if(""+config.auth.enabled === "false") // loose check intentionally
            return next();

        if(/^\/v2\//.test(req.url)) {
            if(req.headers.authorization &&
               /Bearer\ /.test(req.headers.authorization)) {

                // Token supplied
                var m = req.headers.authorization.match(/Bearer\ (.*)/);
                var token = m[1];
                log.info('token supplied', token);
                jwt.verify(token, function(err, data) {
                    if(err) {
                        log.warn('JWT token verify failed', err);
                        res.json(401, 'Not Authorized');
                        return res.end();
                    }
                    if(!data.claims)
                        return res.json(401, 'Not Authorized');
                    // TODO: Verify claims (?)
                    log.trace('Decoded JWT token', data);
                    req.auth = data;
                    return next();
                });
            }
            else {
                var toks = req.url.split(/\//);
                var repo = '*';
                if(toks && toks.length > 1) {
                    repo = toks[2];
                    if(toks.length > 2)
                        repo += '/' + toks[3];
                }
                res.writeHead(401, {'WWW-Authenticate': 'Bearer realm="' + config.auth.realm + '",service="' + config.auth.service+ '", scope="repository:' + repo + ':pull,push"'});
                res.end();
            }
        } else {
            // v1 requests are not protected by JWT token, neither do we have a way to
            // verify the basic auth (yet).
            next();
        }
    });
};
