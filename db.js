/**
   This file is part of node-docker-registry.

   node-docker-registry is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   node-docker-registry is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with node-docker-registry.  If not, see <http://www.gnu.org/licenses/>.
*/

module.exports = function(config, top_log) {

    var mongo = require('mongoskin');
    var db = mongo.db(config.db.mongo.url, {native_parser:true});
    var log = top_log.child({component: 'db'});
    db.bind('blob').bind({
        get_by_digest: function(digest, cb) {
            var q = {digest:digest};
            log.trace("blob.get_by_digest", q);
            this.findOne(q, cb);
        },
        get_by_id: function(id, cb) {
            var q = {id:id};
            log.trace("blob.get_by_id", q);
            this.findOne(q, cb);
        }
    });
    db.blob.createIndex( { "digest": "text" } );

    db.bind('manifest').bind({
        get_by_id: function(id, cb) {
            var q = {id: id};
            log.trace("manifest.get_by_id", q);
            this.findOne(q, cb);
        },
        get_by_repository_name_tag: function(repository, name, tag, cb) {
            var q = {repository: repository,
                     name: name,
                     tag: tag};
            log.trace("manifest.get_by_repository_name_tag", q);
            this.findOne(q, cb);
        }
    });
    db.manifest.ensureIndex( { "name": "text" } );
    
    return db;
};
